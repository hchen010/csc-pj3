#include <iostream>
#include <vector>
using namespace std;



vector<vector<char> > oldB;//old 2d vector
vector<vector<char> > newB; // new 2d vector
const char died='.';
const char alive='0';
const int ROWS=10;
const int COLS=10;

void vectorPrint(const vector<vector<char> > &);
void testStatus(vector<vector<char> > &);
void vectorAssign(vector<vector<char> > &);
void setNext(int,int,char,vector<vector<char> > &);
int neightborTest( int, int,vector<vector<char> > &);


int main()
{

    vectorAssign(oldB);
    vectorAssign(newB);


    oldB[2][3]=alive;
    oldB[2][4]=alive;
    oldB[3][3]=alive;
    oldB[3][5]=alive;
    oldB[2][5]=alive;

    vectorPrint(oldB);
    testDied(oldB);
    testAlive(oldB);
    vectorPrint(newB);

    //works cout<<neightborTest(2,3,oldB);


}

void vectorAssign(vector<vector<char> > &old)
{
    for(int i=0;i<ROWS;i++)
    {
        vector<char>temp;
        for(int j=0;j<COLS;j++)
        {
            temp.push_back(died);
        }
        old.push_back(temp);
    }


}

void vectorPrint(const vector<vector<char> > &old1)
{
    for(int i=0;i<ROWS;i++)
    {
        for(int j=0;j<COLS;j++)
        {
            cout<<old1[i][j];
        }
        cout<<endl;
    }


}


void testAlive(vector<vector<char> > &W)
{

    for(int i=1;i<ROWS;i++)
    {
        for(int j=1;j<COLS;j++)
        {

            if(W[i][j]==alive)


            {

                if((neightborTest(i,j,oldB))>3 || (neightborTest(i,j,oldB)<2))
                {
                    setNext(i,j,died,newB);


                }
                else
                {
                    setNext(i,j,alive,newB);


                }

            }
            #if 0
            if(W[i][j]=died)   //works
            {

                if((neightborTest(i,j,oldB))==3) //error with int i;
                {
                    setNext(i,j,alive,newB);

                }



         }
            #endif
    }

    }


}


void testDied(vector<vector<char> > &W)
{

    for(int i=1;i<ROWS;i++)
    {
        for(int j=1;j<COLS;j++)
        {

            if(W[i][j]==died)
            {
                if((neightborTest(i,j,oldB))==3) //error with int i;
                {
                    setNext(i,j,alive,newB);

                }
            }
        }
    }
}

int neightborTest(int i,int j,vector<vector<char> > &W2) //works as intended
{
  int aliveCount=0;
  if(W2[i-1][j-1]==alive)
  {
      aliveCount++;
  }
  if(W2[i-1][j]==alive)
  {
      aliveCount++;
  }
  if(W2[i-1][j+1]==alive)
  {
      aliveCount++;
  }
  if(W2[i][j-1]==alive)
  {
      aliveCount++;
  }
  if(W2[i][j+1]==alive)
  {
      aliveCount++;
  }
  if(W2[i+1][j-1]==alive)
  {
      aliveCount++;
  }
  if(W2[i+1][j]==alive)
  {
      aliveCount++;
  }
  if(W2[i+1][j+1]==alive)
  {
      aliveCount++;
  }

  return aliveCount;

}

void setNext(int setRow,int setCol,char status,vector<vector<char> > &newB)
{
    newB[setRow][setCol]=status;
}

